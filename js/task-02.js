/*
Фільтрація за діапазоном
https://uk.javascript.info/task/filter-range


Напишіть функцію filterRange(arr, a, b), яка приймає масив arr, шукає в ньому
елементи більші-рівні a та менші-рівні b і віддає масив цих елементів.

Функція повинна повертати новий масив і не змінювати вихідний.
*/

// function filterRange(arr, a, b) {
//   /* ваш код */
// }

// const arr = [5, 3, 8, 1];

// const filtered = filterRange(arr, 1, 4);

// console.log( filtered ); // 3,1 (відфільтровані значення)

// console.log( arr ); // 5,3,8,1 (не змінюється)
