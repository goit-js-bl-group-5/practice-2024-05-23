/*
Вирахувати середній вік
https://uk.javascript.info/task/average-age


Напишіть функцію getAverageAge(users), яка приймає масив об’єктів з властивістю
age та повертає середній вік.

Формула обчислення середнього арифметичного значення:
(age1 + age2 + ... + ageN) / N.
*/

// function getAverageAge(users) {
//   /* ваш код */
// }

// const john = { name: "John", age: 25 };
// const pete = { name: "Pete", age: 30 };
// const mary = { name: "Mary", age: 29 };

// const arr = [ john, pete, mary ];

// console.log( getAverageAge(arr) ); // (25 + 30 + 29) / 3 = 28
