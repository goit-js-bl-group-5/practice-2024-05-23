/*
Скопіювати і впорядкувати масив
https://uk.javascript.info/task/copy-sort-array


У нас є масив рядків arr. Потрібно отримати відсортовану копію та залишити
arr незміненим.

Створіть функцію copySorted(arr), яка буде повертати таку копію.
*/

// function copySorted(arr) {
//   /* ваш код */
// }

// const arr = ["HTML", "JavaScript", "CSS"];

// const sorted = copySorted(arr);

// console.log( sorted ); // CSS, HTML, JavaScript
// console.log( arr ); // HTML, JavaScript, CSS (без змін)
