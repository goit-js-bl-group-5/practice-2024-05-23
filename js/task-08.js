/*
Відсортувати користувачів за віком
https://uk.javascript.info/task/sort-objects


Напишіть функцію sortByAge(users), яка приймає масив обʼєктів з властивістю age
і сортує їх по ньому.
*/

// function sortByAge(users) {
//   /* ваш код */
// }

// const ivan = { name: "Іван", age: 25 };
// const petro = { name: "Петро", age: 30 };
// const mariya = { name: "Марія", age: 28 };

// const arr = [ petro, ivan, mariya ];

// sortByAge(arr);

// // now: [ivan, mariya, petro]
// console.log(arr[0].name); // Іван
// console.log(arr[1].name); // Марія
// console.log(arr[2].name); // Петро
