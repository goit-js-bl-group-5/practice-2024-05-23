/*
Переведіть текст виду border-left-width в borderLeftWidth
https://uk.javascript.info/task/camelcase


Напишіть функцію camelize(str), яка перетворює такі рядки “my-short-string”
в “myShortString”.

Тобто дефіси видаляються, а всі слова після них починаються з великої літери.

P.S. Підказка: використовуйте split, щоб розбити рядок на масив символів, потім переробіть все як потрібно та методом join зʼєднайте елементи в рядок.
*/

// function camelize(str) {
//   /* ваш код */
// }

// console.log(camelize('background-color')); // backgroundColor
// console.log(camelize('list-style-image')); // listStyleImage
// console.log(camelize('-webkit-transition')); // WebkitTransition
