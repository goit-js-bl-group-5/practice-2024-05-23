/*
Фільтрація за діапазоном "на місці"
https://uk.javascript.info/task/filter-range-in-place


Напишіть функцію filterRangeInPlace(arr, a, b), яка приймає масив arr і видаляє
з нього всі значення крім тих, які знаходяться між a і b. Тобто, перевірка має
вигляд a ≤ arr[i] ≤ b.

Функція повинна змінювати поточний масив і нічого не повертати.
*/

// function filterRangeInPlace(arr, a, b) {
//   /* ваш код */
// }

// const arr = [5, 3, 8, 1];

// filterRangeInPlace(arr, 1, 4); // видаляє всі числа крім тих, що в діапазоні від 1 до 4

// console.log( arr ); // [3, 1]
