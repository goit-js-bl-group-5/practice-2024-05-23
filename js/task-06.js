/*
Трансформувати в масив імен
https://uk.javascript.info/task/array-get-names


У вас є масив об’єктів user, і в кожному з них є user.name. Напишіть код, який
перетворює їх в масив імен.
*/

// const ivan = { name: "Іван", age: 25 };
// const petro = { name: "Петро", age: 30 };
// const mariya = { name: "Марія", age: 28 };

// const users = [ ivan, petro, mariya ];

// const names = /* ... ваш код */

// console.log( names ); // Іван, Петро, Марія
